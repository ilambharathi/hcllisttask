//
//  UITableViewCell+Additions.swift
//  HclListTask
//
//  Created by bharathy on 13/07/20.
//  Copyright © 2020 bharathy. All rights reserved.
//

import UIKit

extension UITableViewCell {
    
    static var reuseIdentifier: String! {
        
        let className = String(describing: self)
        
        return className
    }
}
