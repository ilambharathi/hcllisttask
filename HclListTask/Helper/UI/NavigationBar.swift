//
//  SharedClass.swift
//  QuickNews
//
//  Created by bharathy on 14/03/20.
//  Copyright © 2020 bharathy. All rights reserved.
//

import Foundation
import UIKit

class NavigationBar {
    
    // MARK: - Properties
    
    weak var titleLabel: UILabel! = nil
    var navigationHeight: NSLayoutConstraint?
    
    // MARK: - UI and Constraints Method
    
    public func setNavigationBar(superView : UIView) {
        
        let navigationBar = UIView()
        navigationBar.translatesAutoresizingMaskIntoConstraints = false
        navigationBar.backgroundColor = UIColor.init(red: 14.0/255, green: 26.0/255, blue: 166.0/255, alpha: 1)
        superView.addSubview(navigationBar)
        
        let titleLabel = UILabel()
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.textAlignment = .center
        titleLabel.font = UIFont.boldSystemFont(ofSize: Ratio.propValue(value: 20))
        titleLabel.textColor = UIColor.white
        navigationBar.addSubview(titleLabel)
        self.titleLabel = titleLabel
        
        NSLayoutConstraint.activate([
            navigationBar.topAnchor.constraint(equalTo:   navigationBar.superview!.topAnchor, constant: 0),
            navigationBar.leftAnchor.constraint(equalTo:   navigationBar.superview!.leftAnchor, constant: 0),
            navigationBar.rightAnchor.constraint(equalTo:   navigationBar.superview!.rightAnchor, constant: 0),
            
            titleLabel.leftAnchor.constraint(equalTo:   titleLabel.superview!.leftAnchor, constant: 0),
            titleLabel.rightAnchor.constraint(equalTo:   titleLabel.superview!.rightAnchor, constant: 0),
            titleLabel.heightAnchor.constraint(equalToConstant: Ratio.propValue(value: 40)),
            titleLabel.bottomAnchor.constraint(equalTo:   titleLabel.superview!.bottomAnchor, constant: Ratio.propValue(value: -5))
        ])
        
        navigationHeight = navigationBar.heightAnchor.constraint(equalToConstant: self.getNavigationHeight())
        navigationHeight?.isActive = true
    }
    
    // MARK: - Public Method
    
    public func getNavigationHeight() -> CGFloat {
        guard let keyWindow = UIApplication.shared.keyWindow else { return 0 }
        keyWindow.backgroundColor = UIColor.white
        var navBarHeight:CGFloat?
        
        if #available(iOS 11.0, *) {
            navBarHeight = keyWindow.safeAreaInsets.top + Ratio.propValue(value: 60)
        } else {
            navBarHeight = Ratio.propValue(value: 60)
        }
        return navBarHeight!
    }
}

