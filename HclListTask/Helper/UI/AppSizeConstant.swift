//
//  AppSizeConstant.swift
//  HclListTask
//
//  Created by bharathy on 15/07/20.
//  Copyright © 2020 bharathy. All rights reserved.
//

import UIKit
import Foundation

class Ratio {

    // MARK: List of Constants
    static let SCREEN_WIDTH  = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
    
    class func propValue(value: CGFloat) -> CGFloat {
        
        let ratio = (value / 414)
        return Ratio.SCREEN_WIDTH * ratio
    }
}
