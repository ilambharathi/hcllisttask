//
//  ViewController.swift
//  HclListTask
//
//  Created by bharathy on 13/07/20.
//  Copyright © 2020 bharathy. All rights reserved.
//

import UIKit
import CoreData

class ListViewController: UIViewController {
    
    // MARK: - Properties
    
    private var  navigationBar = NavigationBar()
    private var webService = WebService()
    private weak var tableview: UITableView!
    private let refreshControl = UIRefreshControl()
    private var tableArray : [Rows]?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpView()
        fetchData()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator:
       UIViewControllerTransitionCoordinator) {
           super.viewWillTransition(to: size, with: coordinator)
           navigationBar.navigationHeight?.constant = navigationBar.getNavigationHeight()
       }
 
    // MARK: - UI and Constraints Methods
   
    private func setUpView() {
        
        navigationBar.setNavigationBar(superView : self.view)
    
        let tableView = UITableView(frame: .zero, style: .plain)
        tableView.translatesAutoresizingMaskIntoConstraints  = false
        tableView.backgroundColor = .white
        tableView.register(ListTableCell.self, forCellReuseIdentifier: ListTableCell.reuseIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.bounces = true
        tableView.showsVerticalScrollIndicator = false
        tableView.showsHorizontalScrollIndicator = false
        view.addSubview(tableView)
        self.tableview = tableView
      
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshAction(_:)), for: .valueChanged)
        refreshControl.tintColor = UIColor(red:0.25, green:0.72, blue:0.85, alpha:1.0)
        let attributes = [ NSAttributedString.Key.foregroundColor: UIColor.black ]
        refreshControl.attributedTitle = NSAttributedString(string: "Refreshing ...", attributes: attributes)
        
        setupConstraints()
        self.tableReload()
    }
    
    private func setupConstraints() {
        
        let safeArea:UILayoutGuide!
        if #available(iOS 11.0, *) {
            safeArea = tableview.superview?.safeAreaLayoutGuide
        } else{
            safeArea = tableview.superview?.layoutMarginsGuide
        }
        
        NSLayoutConstraint.activate([
            tableview.topAnchor.constraint(equalTo: safeArea.topAnchor,constant : Ratio.propValue(value: 60)),
            tableview.leftAnchor.constraint(equalTo: tableview.superview!.leftAnchor,constant : 0),
            tableview.rightAnchor.constraint(equalTo: tableview.superview!.rightAnchor,constant : 0),
            tableview.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor,constant : 0),
        ])
    }
    
    // MARK: - Private Methods
    
    private func fetchData()
     {
         webService.getWebservice(completion: { (success, error) in
             if(success) {
                 self.tableReload()
             } else {
                self.alertView(message: error)
            }
         })
     }
    
    private func tableReload() {
        DispatchQueue.main.async {
            self.tableArray = Rows.fetchAllRowRequest()
            let countryDetails = CountryDetails.fetchCountryRequest()
            self.navigationBar.titleLabel.text = countryDetails?.title
            self.tableview.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
    
    @objc private func refreshAction(_ sender: Any) {
        // Refresh Data
        self.fetchData()
    }
    
    private func alertView(message: String) {
        
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

//MARK:- Tableview Delegate and Datasource

extension ListViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return Rows.fetchAllRowRequest().count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ListTableCell.reuseIdentifier, for: indexPath) as! ListTableCell
        let rows =  Rows.fetchAllRowRequest()
        cell.rowsDetail = rows[indexPath.row]
        return cell
    }
}


