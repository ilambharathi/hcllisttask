//
//  BaseObject.swift
//  HclListTask
//
//  Created by bharathy on 13/07/20.
//  Copyright © 2020 bharathy. All rights reserved.
//

import UIKit
import CoreData

extension NSManagedObject {
    
    class func entityName() -> String {
        
        return NSStringFromClass(self)
    }
}
