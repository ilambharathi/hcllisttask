//
//  CountryDetails+CoreDataProperties.swift
//  HclListTask
//
//  Created by bharathy on 14/07/20.
//  Copyright © 2020 bharathy. All rights reserved.
//
//

import Foundation
import CoreData


extension CountryDetails {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CountryDetails> {
        return NSFetchRequest<CountryDetails>(entityName: "CountryDetails")
    }

    @NSManaged public var title: String?
    @NSManaged public var rows: NSOrderedSet?

}

// MARK: Generated accessors for rows
extension CountryDetails {

    @objc(insertObject:inRowsAtIndex:)
    @NSManaged public func insertIntoRows(_ value: Rows, at idx: Int)

    @objc(removeObjectFromRowsAtIndex:)
    @NSManaged public func removeFromRows(at idx: Int)

    @objc(insertRows:atIndexes:)
    @NSManaged public func insertIntoRows(_ values: [Rows], at indexes: NSIndexSet)

    @objc(removeRowsAtIndexes:)
    @NSManaged public func removeFromRows(at indexes: NSIndexSet)

    @objc(replaceObjectInRowsAtIndex:withObject:)
    @NSManaged public func replaceRows(at idx: Int, with value: Rows)

    @objc(replaceRowsAtIndexes:withRows:)
    @NSManaged public func replaceRows(at indexes: NSIndexSet, with values: [Rows])

    @objc(addRowsObject:)
    @NSManaged public func addToRows(_ value: Rows)

    @objc(removeRowsObject:)
    @NSManaged public func removeFromRows(_ value: Rows)

    @objc(addRows:)
    @NSManaged public func addToRows(_ values: NSOrderedSet)

    @objc(removeRows:)
    @NSManaged public func removeFromRows(_ values: NSOrderedSet)

}
