//
//  CountryDetails+CoreDataClass.swift
//  HclListTask
//
//  Created by Gurudharan on 14/07/20.
//  Copyright © 2020 bharathy. All rights reserved.
//
//

import Foundation
import CoreData
import UIKit

@objc(CountryDetails)
public class CountryDetails: NSManagedObject {
    
    public class func fetchRequest(for title: String) -> CountryDetails {
        
        let viewContext = AppDelegate.getContext()
        let request = CountryDetails.fetchRequest() as NSFetchRequest
        let predicate = NSPredicate(format: "%K = %@", NSStringFromSelector(#selector(getter: self.title)), title)
        request.predicate = predicate
        request.fetchLimit = 1
        let fetchResults = try! viewContext.fetch(request)
    
        guard let country = fetchResults.first else {
            
            let countryDetail = NSEntityDescription.insertNewObject(forEntityName: CountryDetails.entityName(), into: viewContext) as! CountryDetails
            countryDetail.title = title
            return countryDetail
        }
        return country
    }
    
    public class func fetchCountryRequest() -> CountryDetails? {
        let viewContext = AppDelegate.getContext()
        let request = CountryDetails.fetchRequest() as NSFetchRequest
        let fetchResults = try! viewContext.fetch(request)
        return fetchResults.first
    }
    
    public func populateWithRepresentation(representation: ListModel) {
        
        if let rowsArray = representation.lists {
            for rowItem in rowsArray {
                if let title = rowItem.title {
                    let item = Rows.fetchRequest(for: title)
                    item.populateWithRepresentation(representation: rowItem)
                    self.addToRows(item)
                }
            }
        }
    }
}
