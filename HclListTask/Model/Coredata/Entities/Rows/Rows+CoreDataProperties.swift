//
//  Rows+CoreDataProperties.swift
//  HclListTask
//
//  Created by bharathy on 14/07/20.
//  Copyright © 2020 bharathy. All rights reserved.
//
//

import Foundation
import CoreData


extension Rows {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Rows> {
        return NSFetchRequest<Rows>(entityName: "Rows")
    }

    @NSManaged public var descriptions: String?
    @NSManaged public var imageUrl: String?
    @NSManaged public var title: String?
    @NSManaged public var countryDetail: NSOrderedSet?

}

// MARK: Generated accessors for countryDetail
extension Rows {

    @objc(insertObject:inCountryDetailAtIndex:)
    @NSManaged public func insertIntoCountryDetail(_ value: CountryDetails, at idx: Int)

    @objc(removeObjectFromCountryDetailAtIndex:)
    @NSManaged public func removeFromCountryDetail(at idx: Int)

    @objc(insertCountryDetail:atIndexes:)
    @NSManaged public func insertIntoCountryDetail(_ values: [CountryDetails], at indexes: NSIndexSet)

    @objc(removeCountryDetailAtIndexes:)
    @NSManaged public func removeFromCountryDetail(at indexes: NSIndexSet)

    @objc(replaceObjectInCountryDetailAtIndex:withObject:)
    @NSManaged public func replaceCountryDetail(at idx: Int, with value: CountryDetails)

    @objc(replaceCountryDetailAtIndexes:withCountryDetail:)
    @NSManaged public func replaceCountryDetail(at indexes: NSIndexSet, with values: [CountryDetails])

    @objc(addCountryDetailObject:)
    @NSManaged public func addToCountryDetail(_ value: CountryDetails)

    @objc(removeCountryDetailObject:)
    @NSManaged public func removeFromCountryDetail(_ value: CountryDetails)

    @objc(addCountryDetail:)
    @NSManaged public func addToCountryDetail(_ values: NSOrderedSet)

    @objc(removeCountryDetail:)
    @NSManaged public func removeFromCountryDetail(_ values: NSOrderedSet)

}
