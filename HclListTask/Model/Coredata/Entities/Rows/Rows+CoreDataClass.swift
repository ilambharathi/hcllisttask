//
//  Rows+CoreDataClass.swift
//  HclListTask
//
//  Created by Gurudharan on 14/07/20.
//  Copyright © 2020 bharathy. All rights reserved.
//
//

import Foundation
import UIKit
import CoreData

@objc(Rows)
public class Rows: NSManagedObject {
    
    public class func fetchRequest(for title: String) -> Rows {
        
        let viewContext = AppDelegate.getContext()
        let request = Rows.fetchRequest() as NSFetchRequest
        let predicate = NSPredicate(format: "%K = %@", NSStringFromSelector(#selector(getter: self.title)), title)
        request.predicate = predicate
        request.fetchLimit = 1
        let fetchResults = try! viewContext.fetch(request)
        
        guard let rows = fetchResults.first else {
            
            let rowsDetail = NSEntityDescription.insertNewObject(forEntityName: Rows.entityName(), into: viewContext) as! Rows
            rowsDetail.title = title
            return rowsDetail
        }
        return rows
    }
    
    public class func fetchAllRowRequest() -> [Rows] {
        let viewContext = AppDelegate.getContext()
        let request = Rows.fetchRequest() as NSFetchRequest
        let titleSort = NSSortDescriptor(key:"title", ascending:true)
        request.sortDescriptors = [titleSort]
        let fetchResults = try! viewContext.fetch(request)
        return fetchResults
    }
    
    public func populateWithRepresentation(representation: DetailsRespresentation) {
        
        self.descriptions = representation.description
        self.imageUrl = representation.imageUrl
    }
}
