//
//  WebService.swift
//  HclListTask
//
//  Created by bharathy on 13/07/20.
//  Copyright © 2020 bharathy. All rights reserved.
//

import Foundation
import UIKit

class WebService
{
    func getWebservice(completion: @escaping (Bool, String) -> Void)
    {
        let Url = String(format: "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json")
        guard let serviceUrl = URL(string: Url) else { return }
        
        var request = URLRequest(url: serviceUrl)
        request.httpMethod = "GET"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            
            if let data = data {
                guard let string = String(data: data, encoding: String.Encoding.isoLatin1) else {
                    return
                }
                guard let properData = string.data(using: .utf8, allowLossyConversion: true) else {
                    return
                }
                do {
                    let json = try JSONDecoder().decode(ListModel.self, from: properData)
                    let countryDetail = CountryDetails.fetchRequest(for: json.title ?? "")
                    countryDetail.populateWithRepresentation(representation: json)
                    completion(true, "")
                } catch {
                    completion(false, error.localizedDescription)
                }
            }
            else
            {
                completion(false, error?.localizedDescription ?? "")
            }
        }.resume()
    }
}


