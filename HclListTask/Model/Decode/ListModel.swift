//
//  Modal.swift
//  HclListTask
//
//  Created by bharathy on 14/07/20.
//  Copyright © 2020 bharathy. All rights reserved.
//

import Foundation

public struct ListModel : Decodable {
    
    var title: String?
    var lists: [DetailsRespresentation]?
    
    enum CodingKeys: String, CodingKey {
        
        case title
        case lists = "rows"
    }
}

public struct DetailsRespresentation : Decodable {
    
    var title: String?
    var description: String?
    var imageUrl : String?
    
    enum CodingKeys: String, CodingKey {

        case title = "title"
        case description = "description"
        case imageUrl = "imageHref"
    }
}
