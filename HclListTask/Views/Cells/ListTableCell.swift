//
//  HomeViewTableCellTableViewCell.swift
//  QuickNews
//
//  Created by bharathy on 14/03/20.
//  Copyright © 2020 bharathy. All rights reserved.
//

import UIKit
import SDWebImage

class ListTableCell: UITableViewCell {
    
    // MARK: - Properties
    
    fileprivate var hasSetupConstraints: Bool = false
    private weak var bgView: UIView!
    private weak var imageBanner: UIImageView!
    private weak var labelTitle: UILabel!
    private weak var labelDescription: UILabel!
    private weak var separatorView: UIView!
    
    var rowsDetail: Rows? {
        didSet {
            labelTitle.text = rowsDetail?.title
            labelDescription.text = rowsDetail?.descriptions
            imageBanner.sd_setImage(with: URL(string: rowsDetail?.imageUrl ?? ""), placeholderImage: UIImage(named: "placeholder"))
        }
    }
    
    // MARK: - Lifecycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - UI and Constraints methods
    
    func setupViews() {
        selectionStyle = .none
        
        let bgView = UIView()
        bgView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(bgView)
        
        let imageBanner = UIImageView()
        imageBanner.translatesAutoresizingMaskIntoConstraints = false
        imageBanner.backgroundColor = UIColor.clear
        imageBanner.contentMode = .scaleAspectFit
        bgView.addSubview(imageBanner)
        
        let labelTitle = UILabel()
        labelTitle.translatesAutoresizingMaskIntoConstraints = false
        labelTitle.backgroundColor = UIColor.clear
        labelTitle.textColor = .black
        labelTitle.font = UIFont.systemFont(ofSize: Ratio.propValue(value: 18))
        labelTitle.numberOfLines = 0
        labelTitle.lineBreakMode = .byWordWrapping
        bgView.addSubview(labelTitle)
        
        let labelDescription = UILabel()
        labelDescription.translatesAutoresizingMaskIntoConstraints = false
        labelDescription.backgroundColor = UIColor.clear
        labelDescription.font = UIFont.systemFont(ofSize: Ratio.propValue(value: 16))
        labelDescription.textColor = .black
        labelDescription.numberOfLines = 0
        labelDescription.lineBreakMode = .byWordWrapping
        bgView.addSubview(labelDescription)
        
        let separatorView = UIView()
        separatorView.translatesAutoresizingMaskIntoConstraints = false
        separatorView.backgroundColor = UIColor.lightGray
        contentView.addSubview(separatorView)
        
        self.bgView = bgView
        self.imageBanner = imageBanner
        self.labelTitle = labelTitle
        self.labelDescription = labelDescription
        self.separatorView = separatorView
    }
    
    fileprivate func setupConstraints() {
        NSLayoutConstraint.activate([
            
            bgView.leftAnchor.constraint(equalTo: bgView.superview!.leftAnchor, constant: Ratio.propValue(value: 10)),
            bgView.rightAnchor.constraint(equalTo: bgView.superview!.rightAnchor, constant: Ratio.propValue(value: -10)),
            bgView.topAnchor.constraint(equalTo: bgView.superview!.topAnchor, constant: Ratio.propValue(value: 10)),
            bgView.heightAnchor.constraint(greaterThanOrEqualToConstant: Ratio.propValue(value: 80)),
            
            imageBanner.leftAnchor.constraint(equalTo: imageBanner.superview!.leftAnchor, constant: 0),
            imageBanner.topAnchor.constraint(equalTo: imageBanner.superview!.topAnchor, constant: 0),
            imageBanner.widthAnchor.constraint(equalToConstant: Ratio.propValue(value: 70)),
            imageBanner.heightAnchor.constraint(equalToConstant: Ratio.propValue(value: 70)),
            
            labelTitle.topAnchor.constraint(equalTo: labelTitle.superview!.topAnchor, constant: Ratio.propValue(value: 5)),
            labelTitle.leftAnchor.constraint(equalTo: imageBanner.rightAnchor, constant: Ratio.propValue(value: 10)),
            labelTitle.rightAnchor.constraint(equalTo: labelTitle.superview!.rightAnchor, constant: 0),
            labelTitle.heightAnchor.constraint(greaterThanOrEqualToConstant: Ratio.propValue(value: 20)),
            
            labelDescription.topAnchor.constraint(equalTo: labelTitle.bottomAnchor, constant: Ratio.propValue(value: 5)),
            labelDescription.leftAnchor.constraint(equalTo: imageBanner.rightAnchor, constant: Ratio.propValue(value: 10)),
            labelDescription.rightAnchor.constraint(equalTo: labelDescription.superview!.rightAnchor, constant: 0),
            labelDescription.heightAnchor.constraint(greaterThanOrEqualToConstant: Ratio.propValue(value: 20)),
            labelDescription.bottomAnchor.constraint(lessThanOrEqualTo: labelDescription.superview!.bottomAnchor, constant: Ratio.propValue(value: -1)),
            
            separatorView.topAnchor.constraint(equalTo: bgView.bottomAnchor, constant: Ratio.propValue(value: 10)),
            separatorView.leftAnchor.constraint(equalTo: separatorView.superview!.leftAnchor, constant: Ratio.propValue(value: 5)),
            separatorView.rightAnchor.constraint(equalTo: separatorView.superview!.rightAnchor, constant: Ratio.propValue(value: -5)),
            separatorView.heightAnchor.constraint(equalToConstant: Ratio.propValue(value: 1)),
            separatorView.bottomAnchor.constraint(equalTo: separatorView.superview!.bottomAnchor, constant:0)
        ])
    }
    
    override class var requiresConstraintBasedLayout: Bool {
        return true
    }
    
    override func prepareForReuse() {
        self.imageBanner.image = UIImage(named: "placeholder")
        labelTitle.text = ""
        labelDescription.text = ""
    }
}

